python main_sync.py
-------------------

Output:
http://ya.ru [start]
http://ya.ru [finish]
http://google.com [start]
http://google.com [finish]
http://mail.ru [start]
http://mail.ru [finish]
http://rambler.ru [start]
http://rambler.ru [finish]
http://rbc.ru [start]
http://rbc.ru [finish]
Time spent: 0:00:02.417229


python main_async.py
--------------------

Output:
http://ya.ru [start]
http://google.com [start]
http://mail.ru [start]
http://rambler.ru [start]
http://rbc.ru [start]
http://ya.ru [finish]
http://mail.ru [finish]
http://rambler.ru [finish]
http://rbc.ru [finish]
http://google.com [finish]
Time spent: 0:00:01.497803