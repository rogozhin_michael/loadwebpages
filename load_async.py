import aiohttp
import asyncio
import os
import urllib.request
import zipfile

from utils import get_filename


async def load_web_page(url):
    print(url, '[start]')
    filename = get_filename(url)
    filename_zip = filename.replace('.html', '.zip')

    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:  #  <class 'aiohttp.client_reqrep.ClientResponse'>
            data = await response.read()

    f = open(filename, 'wb')
    f.write(data)
    f.close()

    with zipfile.ZipFile(filename_zip, 'w', compression=zipfile.ZIP_STORED) as zipf:
        zipf.write(filename) # compresslevel=2
    os.remove(filename)
    print(url, '[finish]')
