import os
import urllib.request
import zipfile

from utils import get_filename


def load_web_page(url):
    print(url, '[start]')
    filename = get_filename(url)
    filename_zip = filename.replace('.html', '.zip')
    resp = urllib.request.urlopen(url)  # <class 'http.client.HTTPResponse'>
    data = resp.read()  # bytes

    f = open(filename, 'wb')
    f.write(data)
    f.close()

    with zipfile.ZipFile(filename_zip, 'w', compression=zipfile.ZIP_STORED) as zipf:
        zipf.write(filename) # compresslevel=2
    os.remove(filename)
    print(url, '[finish]')
