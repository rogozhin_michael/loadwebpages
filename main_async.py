import asyncio
import datetime

from config import URLS
from load_async import load_web_page


dt1 = datetime.datetime.now()

async def main():
    tasks = []
    for url in URLS:
        task = asyncio.create_task(load_web_page(url))
        #task.set_name('Task: ' + url)
        tasks.append(task)
    for task in tasks:
        await task

asyncio.run(main())

dt2 = datetime.datetime.now()
time_delta = dt2 - dt1

print(f'Time spent: {time_delta}')
