import datetime

from config import URLS
from load_sync import load_web_page


dt1 = datetime.datetime.now()

for url in URLS:
    load_web_page(url)

dt2 = datetime.datetime.now()
time_delta = dt2 - dt1

print(f'Time spent: {time_delta}')
