def get_filename(url):
    filename = url.replace('http://', '').replace('https://', '').replace('.', '_')
    filename = 'tmp/' + filename + '.html'
    return filename
